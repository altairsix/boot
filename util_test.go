package boot_test

import (
	"testing"

	"bitbucket.org/altairsix/boot"
	"github.com/stretchr/testify/assert"
)

func TestRel(t *testing.T) {
	testCases := map[string]struct {
		Prefix   string
		Key      string
		Expected string
	}{
		"simple": {
			Prefix:   "/a/b",
			Key:      "/a/b/c",
			Expected: "c",
		},
		"nested": {
			Prefix:   "/a/b",
			Key:      "/a/b/c/d",
			Expected: "c/d",
		},
	}

	for label, tc := range testCases {
		t.Run(label, func(t *testing.T) {
			assert.Equal(t, tc.Expected, boot.Rel(tc.Prefix, tc.Key))
		})
	}
}
