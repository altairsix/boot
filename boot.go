package boot

import (
	"bufio"
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kms"
	"github.com/aws/aws-sdk-go/service/kms/kmsiface"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3iface"
	"github.com/pkg/errors"
)

const (
	// Version is the current app version
	Version = "0.2.0-SNAPSHOT"
)

const (
	// EncSuffix is the suffix files will have in S3 that have been encrypted
	EncSuffix = ".enc"

	// DefaultRevision provides the name of the default revision
	DefaultRevision = "latest"
)

// Client provides the central access
type Client struct {
	s3API    s3iface.S3API
	kmsAPI   kmsiface.KMSAPI
	env      string
	bucket   string
	app      string
	revision string
	writer   io.Writer
	dryrun   bool
}

// HandleFunc allows for the custom processing of files
type HandleFunc func(filename string, r io.Reader) error

// Option provides functional arguments to New
type Option func(*Client)

// DryRun provides an option, when set to true, that goes through the motions but doesn't upload or download files
func DryRun(v bool) Option {
	return func(c *Client) {
		c.dryrun = v
	}
}

// New constructs a new boot Client
func New(region, env, bucket, app, revision string, options ...Option) (*Client, error) {
	cfg := &aws.Config{Region: aws.String(region)}
	s, err := session.NewSession(cfg)
	if err != nil {
		return nil, errors.Wrapf(err, "unable to create aws session for region, %v", region)
	}

	if env == "" {
		return nil, errors.New("please specify the env")
	}
	if bucket == "" {
		return nil, errors.New("please specify the s3 bucket")
	}
	if app == "" {
		return nil, errors.New("please specify the app")
	}

	c := &Client{
		s3API:    s3.New(s),
		kmsAPI:   kms.New(s),
		env:      env,
		bucket:   bucket,
		app:      app,
		revision: revision,
		writer:   os.Stdout,
	}

	for _, opt := range options {
		opt(c)
	}

	return c, nil
}

// PullFile retrieves the file at the specified relative path from S3
func (c *Client) PullFile(ctx context.Context, rel string) (io.ReadCloser, error) {
	key := S3Key(c.env, c.app, c.revision, rel)

	log.Printf("pulling s3://%v/%v\n", c.bucket, key)

	out, err := c.s3API.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(c.bucket),
		Key:    &key,
	})
	if err != nil {
		return nil, errors.Wrapf(err, "failed to retrieve object from s3, %v", key)
	}

	return out.Body, nil
}

// Pull retrieves all the files from S3 and places them into the target directory
func (c *Client) Pull(ctx context.Context, target string, handlers ...HandleFunc) error {
	prefix := S3Key(c.env, c.app, c.revision, "")

	out, err := c.s3API.ListObjects(&s3.ListObjectsInput{
		Bucket: aws.String(c.bucket),
		Prefix: aws.String(prefix),
	})
	if err != nil {
		return errors.Wrapf(err, "failed to retrieve list of objects from s3://%v/%v", c.bucket, prefix)
	}

	h := handleSave()
	if len(handlers) > 0 {
		h = handlers[0]
	}

	for _, item := range out.Contents {
		if strings.HasSuffix(*item.Key, "/") {
			// skip directories
			continue
		}

		rel := Rel(prefix, *item.Key)

		err = c.HandleFile(ctx, rel, target, h)
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *Client) decrypt(r io.ReadCloser) (io.ReadCloser, error) {
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, errors.Wrap(err, "unable to read content for decryption")
	}

	data, err = base64.StdEncoding.DecodeString(string(data))
	if err != nil {
		return nil, errors.Wrap(err, "unable to decode base64 cipher text")
	}

	out, err := c.kmsAPI.Decrypt(&kms.DecryptInput{
		CiphertextBlob: data,
	})
	if err != nil {
		return nil, err
	}

	r.Close()

	return ioutil.NopCloser(bytes.NewReader(out.Plaintext)), nil
}

// HandleFile pulls the file from the specified rel location and saves it to the target directory
func (c *Client) HandleFile(ctx context.Context, rel, target string, fn HandleFunc) error {
	r, err := c.PullFile(ctx, rel)
	if err != nil {
		return err
	}
	defer r.Close()

	filename := filepath.Join(target, rel)
	dir := filepath.Dir(filename)
	err = os.MkdirAll(dir, 0755)
	if err != nil {
		return err
	}

	if strings.HasSuffix(filename, EncSuffix) {
		r, err = c.decrypt(r)
		if err != nil {
			return err
		}
		filename = filename[0 : len(filename)-len(EncSuffix)]
	}

	if c.dryrun {
		return nil
	}

	return fn(filename, r)
}

// Push the contents of the src directory to S3
func (c *Client) Push(ctx context.Context, kmsID, src string) error {
	abs, err := filepath.Abs(src)
	if err != nil {
		return errors.Wrapf(err, "unable to determine absolute path for %v", src)
	}

	revision := Timestamp()

	return filepath.Walk(abs, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}

		rel := Rel(abs, path)

		encryptFile := info.Size() < 4000 && info.Size() > 0

		if err := c.PushFile(ctx, kmsID, DefaultRevision, src, rel, encryptFile); err != nil {
			return errors.Wrapf(err, "unable to push file, %v", rel)
		}
		if err := c.PushFile(ctx, kmsID, revision, src, rel, encryptFile); err != nil {
			return errors.Wrapf(err, "unable to push file, %v", rel)
		}

		return nil
	})
}

func (c *Client) encrypt(kmsID string, seeker io.ReadSeeker) (io.ReadSeeker, error) {
	data, err := ioutil.ReadAll(seeker)
	if err != nil {
		return nil, errors.Wrap(err, "unable to read content to encrypt")
	}

	out, err := c.kmsAPI.Encrypt(&kms.EncryptInput{
		KeyId:     aws.String(kmsID),
		Plaintext: data,
	})
	if err != nil {
		return nil, errors.Wrap(err, "unable to encrypt contents")
	}

	cipherText := base64.StdEncoding.EncodeToString(out.CiphertextBlob)
	return strings.NewReader(cipherText), nil
}

// PushFile pushes a single file to s3
func (c *Client) PushFile(ctx context.Context, kmsID, revision, src, rel string, encryptFile bool) error {
	if kmsID == "" {
		return errors.New("Missing AWS KMS ID")
	}

	key := S3Key(c.env, c.app, revision, rel)
	filename := filepath.Join(src, rel)

	f, err := os.Open(filename)
	if err != nil {
		return errors.Wrapf(err, "unable to read file, %v", filename)
	}
	defer f.Close()

	r := io.ReadSeeker(f)
	if encryptFile {
		r, err = c.encrypt(kmsID, r)
		if err != nil {
			return err
		}
		key += EncSuffix
	}

	log.Printf("copying %v to s3://%v/%v\n", filename, c.bucket, key)
	if c.dryrun {
		return nil
	}
	_, err = c.s3API.PutObject(&s3.PutObjectInput{
		Body:   r,
		Bucket: aws.String(c.bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		return errors.Wrapf(err, "unable to put object, s3://%v/%v", c.bucket, key)
	}

	return nil
}

func (c *Client) Container(ctx context.Context, stdout, stderr io.Writer, target string, args ...string) error {
	err := c.Pull(ctx, target, handleSetenvOrElse(handleSave(), "boot.env", "boot.json"))
	if err != nil {
		return err
	}

	if len(args) == 0 {
		return errors.New("container:err:args")
	}

	log.Println("executing ... ", strings.Join(args, " "))

	cmd := exec.Command(args[0], args[1:]...)
	cmd.Stdout = stdout
	cmd.Stderr = stderr
	return cmd.Run()
	return nil
}

func handleSave() HandleFunc {
	return func(filename string, r io.Reader) error {
		log.Printf("writing file, %v\n", filename)
		f, err := os.OpenFile(filename, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
		if err != nil {
			return err
		}
		defer f.Close()

		_, err = io.Copy(f, r)
		if err != nil {
			return err
		}

		return nil
	}
}

func handleSetenv() HandleFunc {
	return func(filename string, r io.Reader) error {
		data, err := ioutil.ReadAll(r)
		if err != nil {
			return err
		}

		content := map[string]string{}
		ext := filepath.Ext(filename)
		switch ext {
		case ".json":
			if err := json.Unmarshal(data, &content); err != nil {
				return err
			}

		case ".env":
			buf := bufio.NewReader(bytes.NewReader(data))
			for {
				data, _, err := buf.ReadLine()
				if err != nil {
					if err == io.EOF {
						break
					}
					return err
				}

				line := strings.TrimSpace(string(data))
				if line == "" || strings.HasPrefix(line, "#") {
					continue
				}

				segments := strings.SplitN(line, "=", 2)
				if len(segments) == 2 {
					k := strings.TrimSpace(segments[0])
					v := strings.TrimSpace(segments[1])
					content[k] = v
				}
			}
		default:
			return fmt.Errorf("don't know how to set env from files of type, %v", ext)
		}

		for k, v := range content {
			os.Setenv(k, v)
		}

		return nil
	}
}

func handleSetenvOrElse(orElse HandleFunc, filenames ...string) HandleFunc {
	h := handleSetenv()

	return func(filename string, r io.Reader) error {
		base := filepath.Base(filename)
		for _, f := range filenames {
			if f == base {
				return h(filename, r)
			}
		}

		return orElse(filename, r)
	}
}
