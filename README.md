# boot

specialized tool to boot loyal3 docker containers

## Usage

```
NAME:
   main - utility to boot container

USAGE:
   main [global options] command [command options] [arguments...]

VERSION:
   0.1.0-SNAPSHOT

COMMANDS:
     pull       pull configs from s3 to local directory
     push       push configs from local directory to s3
     container  start container
     help, h    Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --help, -h     show help
   --version, -v  print the version
```

### push

Takes the specified directory of configuration data and pushes it to S3, 
encrypting any file smaller than 4k along the way.

```
boot push --dir sample --s3-bucket target-bucket --kms-id 01234556-abcd-0123-abcd-012345678901
```

Arguments can also be passed via environment:

```
NAME:
   main push - push configs from local directory to s3

USAGE:
   main push [command options] [arguments...]

OPTIONS:
   --region value     aws region containing the s3 bucket (default: "us-east-1") [$AWS_DEFAULT_REGION]
   --env value        environment (default: "local") [$BOOT_ENV]
   --app value        the name of the app (default: "app") [$BOOT_APP]
   --s3-bucket value  name of the s3-bucket to read from [$BOOT_S3_BUCKET]
   --dir value        the local directory to read/write to (default: ".") [$BOOT_DIR]
   --verbose          display additional logging [$BOOT_VERBOSE]
   --dryrun           dry run, don't actually make any changes [$BOOT_DRYRUN]
   --kms-id value     AWS KMS key id [$BOOT_KMS_ID]
```

### pull

Essentially the reverse of push.  Retrieves encrypted content in S3 and 
populates the specified directory with that content.

```
go run main.go pull --dir target --s3-bucket target-bucket 
```

Arguments can also be passed via environment:

```
NAME:
   main pull - pull configs from s3 to local directory

USAGE:
   main pull [command options] [arguments...]

OPTIONS:
   --region value     aws region containing the s3 bucket (default: "us-east-1") [$AWS_DEFAULT_REGION]
   --env value        environment (default: "local") [$BOOT_ENV]
   --app value        the name of the app (default: "app") [$BOOT_APP]
   --s3-bucket value  name of the s3-bucket to read from [$BOOT_S3_BUCKET]
   --dir value        the local directory to read/write to (default: ".") [$BOOT_DIR]
   --verbose          display additional logging [$BOOT_VERBOSE]
   --dryrun           dry run, don't actually make any changes [$BOOT_DRYRUN]
   --revision value   the name of the version to use (default: "latest") [$BOOT_REVISION]
```

### container

To be accomplished.

## IAM

Make sure you have credentials to:
 
* Read/Write to the S3 bucket
* Encrypt/Decrypt content using the specified KMS ID
