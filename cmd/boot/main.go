package main

import (
	"context"
	"io"
	"log"
	"os"

	"time"

	"bitbucket.org/altairsix/boot"
	"github.com/savaki/loggly"
	"github.com/urfave/cli"
)

type options struct {
	Region      string
	Env         string
	App         string
	Revision    string
	KMS         string
	S3Bucket    string
	Dir         string
	Verbose     bool
	Dryrun      bool
	LogglyToken string
}

var opts options

func main() {
	flags := []cli.Flag{
		cli.StringFlag{
			Name:        "region",
			Value:       "us-east-1",
			Usage:       "aws region containing the s3 bucket",
			EnvVar:      "AWS_DEFAULT_REGION",
			Destination: &opts.Region,
		},
		cli.StringFlag{
			Name:        "env",
			Usage:       "environment",
			Value:       "local",
			EnvVar:      "BOOT_ENV",
			Destination: &opts.Env,
		},
		cli.StringFlag{
			Name:        "app",
			Usage:       "the name of the app",
			Value:       "app",
			EnvVar:      "BOOT_APP",
			Destination: &opts.App,
		},
		cli.StringFlag{
			Name:        "s3-bucket",
			Usage:       "name of the s3-bucket to read from",
			EnvVar:      "BOOT_S3_BUCKET",
			Destination: &opts.S3Bucket,
		},
		cli.StringFlag{
			Name:        "dir",
			Usage:       "the local directory to read/write to",
			Value:       ".",
			EnvVar:      "BOOT_DIR",
			Destination: &opts.Dir,
		},
		cli.BoolFlag{
			Name:        "verbose",
			Usage:       "display additional logging",
			EnvVar:      "BOOT_VERBOSE",
			Destination: &opts.Verbose,
		},
		cli.BoolFlag{
			Name:        "dryrun",
			Usage:       "dry run, don't actually make any changes",
			EnvVar:      "BOOT_DRYRUN",
			Destination: &opts.Dryrun,
		},
		cli.StringFlag{
			Name:        "token",
			Usage:       "the loggly token",
			EnvVar:      "LOGGLY_TOKEN",
			Destination: &opts.LogglyToken,
		},
	}

	app := cli.NewApp()
	app.Usage = "utility to boot container"
	app.Version = boot.Version
	app.Commands = []cli.Command{
		{
			Name:  "pull",
			Usage: "pull configs from s3 to local directory",
			Flags: append(flags,
				cli.StringFlag{
					Name:        "revision",
					Usage:       "the name of the version to use",
					Value:       boot.DefaultRevision,
					EnvVar:      "BOOT_REVISION",
					Destination: &opts.Revision,
				},
			),
			Action: handle(pull),
		},
		{
			Name:  "push",
			Usage: "push configs from local directory to s3",
			Flags: append(flags,
				cli.StringFlag{
					Name:        "kms-id",
					Usage:       "AWS KMS key id",
					EnvVar:      "BOOT_KMS_ID",
					Destination: &opts.KMS,
				},
			),
			Action: handle(push),
		},
		{
			Name:  "container",
			Usage: "start container",
			Flags: append(flags,
				cli.StringFlag{
					Name:        "revision",
					Usage:       "the name of the version to use",
					Value:       boot.DefaultRevision,
					EnvVar:      "BOOT_REVISION",
					Destination: &opts.Revision,
				},
			),
			Action: handle(container),
		},
	}
	app.Run(os.Args)
}

func handle(fn func(ctx context.Context, c *boot.Client, args ...string) error) cli.ActionFunc {
	return func(c *cli.Context) error {
		client, err := boot.New(opts.Region, opts.Env, opts.S3Bucket, opts.App, opts.Revision,
			boot.DryRun(opts.Dryrun),
		)
		if err != nil {
			log.Fatalln(err)
		}

		err = fn(context.Background(), client, c.Args()...)
		if err != nil {
			log.Fatalln(err)
		}

		return nil
	}
}

func pull(ctx context.Context, c *boot.Client, _ ...string) error {
	return c.Pull(ctx, opts.Dir)
}

func push(ctx context.Context, c *boot.Client, _ ...string) error {
	if opts.KMS == "" {
		log.Fatalln("Please specify the KMS ID via --kms-id")
	}
	return c.Push(ctx, opts.KMS, opts.Dir)
}

func container(ctx context.Context, c *boot.Client, args ...string) error {
	w := io.Writer(os.Stdout)
	if opts.LogglyToken != "" {
		client := loggly.New(opts.LogglyToken, loggly.Interval(time.Second))
		defer client.Flush()
		w = io.MultiWriter(w, client)
	}
	return c.Container(ctx, w, w, opts.Dir, args...)
}
