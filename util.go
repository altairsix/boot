package boot

import (
	"path/filepath"
	"strings"
	"time"
)

// S3Key provides a uniform function for generating a path to the content
func S3Key(env, app, revision, paths string) string {
	return filepath.Join(env, app, revision, paths)
}

// Rel returns the relative path of the key
func Rel(prefix, key string) string {
	// strip the prefix off the front
	key = key[len(prefix):]
	if strings.HasPrefix(key, "/") {
		key = key[1:]
	}
	return key
}

// Timestamp returns the current date in a format suitable for use as a revision
func Timestamp() string {
	return time.Now().Format("2006.01.02-15.04.05")
}
